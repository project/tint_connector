<?php

namespace Drupal\tint_connector\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'tint' field type.
 *
 * @FieldType(
 *   id = "field_tint",
 *   label = @Translation("Tint Widget"),
 *   description = @Translation("Tint to display a
 *   Tint widget."),
 *   category = @Translation("General"),
 *   default_widget = "tint_connector_widget",
 *   default_formatter = "tint_connector_formatter"
 * )
 */
class TintFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'tiny',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('boolean')
      ->setLabel(t('Boolean value'));
    return $properties;
  }

}
