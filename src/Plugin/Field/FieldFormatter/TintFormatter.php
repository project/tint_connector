<?php

namespace Drupal\tint_connector\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Plugin implementation of the 'field_tint' formatter.
 *
 * @FieldFormatter(
 *   id = "tint_connector_formatter",
 *   module = "tint_connector",
 *   label = @Translation("Tint Integration widget formatter"),
 *   field_types = {
 *     "field_tint"
 *   }
 * )
 */
class TintFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $configFactory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $configSettings = $this->configFactory->get('tint_connector.settings');
      // Render each element as markup.
      $element[$delta] = [
        '#theme' => 'tint-widget',
        '#data_id' => $configSettings->get('data_id'),
        '#personalization_id' => $configSettings->get('personalization_id'),
        '#data_expand' => $configSettings->get('data_expand'),
        '#data_infinitescroll' => $configSettings->get('data_infinitescroll'),
        '#columns' => $configSettings->get('columns'),
        '#clickformore' => $configSettings->get('clickformore'),
        '#attached' => [
          'library' => [
            'tint_connector/tint-library',
          ],
        ],
      ];
    }
    return $element;
  }

}
