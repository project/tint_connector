<?php

namespace Drupal\tint_connector\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'tint_connector_widget' widget.
 *
 * @FieldWidget(
 *   id = "tint_connector_widget",
 *   module = "tint_connector",
 *   label = @Translation("Tint Integration Widget"),
 *   field_types = {
 *     "field_tint"
 *   }
 * )
 */
class TintWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : 0;

    $element += [
      '#type' => 'checkbox',
      '#title' => $this->t('Tint widget'),
      '#default_value' => $value,
    ];
    return ['value' => $element];
  }

}
