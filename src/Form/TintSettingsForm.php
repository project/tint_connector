<?php

namespace Drupal\tint_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form builder for the tint settings admin page.
 */
class TintSettingsForm extends ConfigFormBase {
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tint_connector.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tint_connector_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tint_connector.settings');
    $form['personalization_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Personalization Id'),
      '#required' => TRUE,
      '#description' => $this->t('A numeric value.'),
      '#default_value' => $config->get('personalization_id'),
    ];
    $form['data_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data Id'),
      '#required' => TRUE,
      '#description' => $this->t('Enter the machine name of the tint app.'),
      '#default_value' => $config->get('data_id'),
    ];
    $form['clickformore'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Clickformore'),
      '#description' => $this->t('Show "Load More" button.'),
      '#default_value' => $config->get('clickformore'),
      '#required' => TRUE,
      '#options' => [TRUE => $this->t('Yes'), FALSE => $this->t('No')],
    ];
    $form['columns'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Columns'),
      '#description' => $this->t('Number of columns for tint display on UI.'),
      '#default_value' => $config->get('columns'),
      '#required' => TRUE,
      '#options' => [
        1 => $this->t('One Column'),
        2 => $this->t('Two Column'),
        3 => $this->t('Three Column'),
        4 => $this->t('Four Column'),
        5 => $this->t('Five Column'),
        6 => $this->t('Six Column'),
        7 => $this->t('Seven Column'),
        8 => $this->t('Eight Column'),
      ],
    ];
    $form['data_infinitescroll'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Infinitescroll'),
      '#description' => $this->t('Data load on page scroll.'),
      '#default_value' => $config->get('data_infinitescroll'),
      '#required' => TRUE,
      '#options' => [TRUE => $this->t('Yes'), FALSE => $this->t('No')],
    ];
    $form['data_expand'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Expand'),
      '#description' => $this->t('Data expand on the page with default layout.'),
      '#default_value' => $config->get('data_expand'),
      '#required' => TRUE,
      '#options' => [TRUE => $this->t('Yes'), FALSE => $this->t('No')],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('tint_connector.settings')
      ->set('personalization_id', $form_state->getValue('personalization_id'))
      ->set('data_id', $form_state->getValue('data_id'))
      ->set('clickformore', $form_state->getValue('clickformore'))
      ->set('columns', $form_state->getValue('columns'))
      ->set('data_infinitescroll', $form_state->getValue('data_infinitescroll'))
      ->set('data_expand', $form_state->getValue('data_expand'))
      ->save();
  }

}
