------------------------
INTRODUCTION
------------------------
The Tint module integrates the TINT widget with the Drupal website.

Install & configure the Tint Connector module.


INSTALLATION
------------

1) Download the module and place it inside the modules/contrib directory.

2) Enable the Tint connector module from /admin/modules URL.

3) Configuration URL /admin/config/tint_connector/tint_settings

4) Go to any content type and add the Tint Widget field.

5) Go to that Node type page under creating a node form.

6) Fill the required detail and save the Node.
The landing page should reflect Tint Social feeds.

REQUIREMENTS
--------------------

1) Open the TINT website https://www.tintup.com and create an account.

2) Create a new TINT app (https://www.tintup.com/app/).

3) Click on Edit button and add the multiple networks inside the Tint app.

4) Generate the Web Embed code


CONFIGURATION
--------------------

1) Update the tint settings from (/admin/config/tint_connector/tint_settings)
 value from new tint app data.

EXTEND
-------
 - Hook_theme_hook_alter for extending default template of Tint.
 - Override default css libraries to change default UI/UX. 
